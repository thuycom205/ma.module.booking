<?php
/**
* @method string getIsMandatory()
* @method int getCanServerHowmany()
* @method string getIsAutoAssign()
*/
class HN_Book_Model_BResource extends Mage_Core_Model_Abstract {
	
	
	/**
	 * Initialize resource model
	 *
	 */
	protected function _construct()
	{
		$this->_init('book/bresource');
		parent::_construct();
	}
	
	/**
	 * co the phuc vu 3 phong
	 * mot luc
	 * select r.book_id as book_id,r.resource_id as resource_id ,
	 * book.from as from , book.to as to
	 * 
	 *  from relation  as r
	 * left join book as book on  r.book_id = b.book_id
	 * 
	 * where resource_id = and from = and to =
	 * 
	 * 
	 * trong thoi gian tu x den y thi 
	 * dang phuc vu may phong
	 * @description  get 
	 * @param unknown $from
	 * @param unknown $to
	 */
	
	public function  getStatusOption() {
		
		$option = array(
				'available' => __('Available'),
				'unavailable' => __('Un available'),
				
		);
		
		Mage::dispatchEvent('booking_resource_get_statusoption', $option);
		
		return $option;
	}
	
	
}