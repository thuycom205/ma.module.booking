<?php

class HN_Book_Model_Product_Type_Book extends Mage_Catalog_Model_Product_Type_Virtual
{
	public function isVirtual($product = null)
	{
		return true;
	}
	
	public function hasOptions($product = null)
	{
		return true;
		//return $this->getProduct($product)->getLinksPurchasedSeparately()
		//|| parent::hasOptions($product);
	}
	
// 	protected function _prepareProduct(Varien_Object $buyRequest, $product, $processMode) {
// 		Mage::log('prepareproduct' , null, 'booka.log' ,true);
		
// 		$result = parent::_prepareProduct($buyRequest, $product, $processMode);
// 		//return $result;
// 		$preparedTimes = $buyRequest->getBookableTimes();
// 		if (is_string($preparedTimes)) $preparedTimes = array($preparedTimes);
// 		if ($preparedTimes) {
// 			$this->getProduct($product)->addCustomOption('bookable_times', implode(',', $preparedTimes));
// 			Mage::log($preparedTimes , null, 'booka.log' ,true);
// 			return $result;
// 		}
// 	}
	/**
	 * Prepare selected options for downloadable product
	 *
	 * @param  Mage_Catalog_Model_Product $product
	 * @param  Varien_Object $buyRequest
	 * @return array
	 */
	public function processBuyRequest($product, $buyRequest)
	{
		$links = $buyRequest->getBookableTimes();
		//$links = (is_array($links)) ? array_filter($links, 'intval') : array();
	
		$options = array('links' => $links);
	    
		Mage::log('option procesBuyRequest' , null, 'booka.log' ,true);
		
		Mage::log($options , null, 'booka.log' ,true);
		return $options;
	}
	
	
// 	public function getOrderOptions($product = null)
// 	{
// 		Mage::log('get order options enthusiastic enthusiastic true enthusiastic' , null, 'booka.log' ,true); 
// 		$options = parent::getOrderOptions($product);
// 		if ($linkIds = $this->getProduct($product)->getCustomOption('bookable_times')) {
// 			$linkOptions = array();
// 			//$links = $this->getLinks($product);
// 			foreach (explode(',', $linkIds->getValue()) as $linkId) {
// 				//if (isset($links[$linkId])) {
// 					$linkOptions[] = $linkId;
// 				//}
// 			}
// 			$options = array_merge($options, array('booktimes' => $linkOptions));
// 		}
// 		$options = array_merge($options, array(
// 				'is_book' => true,
// 				'real_product_type' => 'book'
// 		));
// 		return $options;
// 	}
	
}