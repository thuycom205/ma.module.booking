<?php
class HN_Book_Model_Product_Price extends Mage_Catalog_Model_Product_Type_Price {

	public function getPrice($product)
	{
		//return 12.8;
		$time_type ='';
		
		/* @var $helper HN_Book_Helper_Data */
		$helper = Mage::helper('book');
		
		$price =  $product->getData('price');
	    $current_time =	Mage::getModel('core/date')->date('Y-m-d H:i:s');
	    $priceRules = Mage::getModel('book/pricerule')->getCollection()->addFilter('product_id' , $product->getId());

	    $satisfied = false;
	    $custom_price = $price;
	    if ($priceRules) {
	    	foreach ($priceRules as $key=>$row) {
	    		
	    		$time_type = $row['time_type'] ;
	    		
	    		$from = $row['time_from'];
	    		
	    		$to = $row['time_to'];
	    		
	    		
	    		$satisfied = $helper->comparedatime($time_type, $from, $to);
	    		if ($satisfied) {
	    			return max(0,$row['price']) ;
	    		}
	    		
	    	}
	    	
	    } else {
	    	return $price;
	    }
		return $product->getData('price');
	}
	
    
}