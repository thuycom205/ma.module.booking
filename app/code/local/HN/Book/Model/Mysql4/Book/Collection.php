<?php
class HN_Book_Model_Mysql4_Book_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract

{
	public function _construct()
	{
		parent::_construct();

		$this->_init('book/book');

	}
	
	public function getPriceRules($product_id) {
		$select = $this->getSelect()->where('product_id=?', $product_id);
		return 	$this->getData();
		
	}
}
