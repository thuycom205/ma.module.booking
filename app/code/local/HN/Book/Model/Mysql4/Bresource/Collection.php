<?php
class HN_Book_Model_Mysql4_Bresource_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract

{
	public function _construct()
	{
		parent::_construct();

		$this->_init('book/bresource');

	}
	
	public function isRelationExist($book_id , $resource_id) {
		
		$this->getSelect()->where('book_id =?' , $book_id)->where('resource_id' , $resource_id);
		return $this;
	}
	
	public function getResourceByProductId($product_id) {
	
		$select = $this->getSelect()->where('product_id=?', $product_id);
		return 	$this;
		
	}
	
	public function getReservingItems($resource_id,$from, $to) {
		$relation_tbl = '';
		$relation_tbl =$this->getTable('book/brrelation');
		
		$book_tbl = $this->getTable('book/book');                                           
		$this->getSelect()->joinLeft(array('r' =>$relation_tbl), 'main_table.id = r.resource_id', array('book_id'))
		->join(array('b' =>$book_tbl), 'r.book_id= b.id')
		 ->where('main_table.id = ?', $resource_id)
		->where('b.from<=?', $from)
		->where('b.to>=?', $to)
		;
		
		return $this;
	}
	public function getPriceRules($product_id) {
		$select = $this->getSelect()->where('product_id=?', $product_id);
		return 	$this->getData();
		
	}
}
