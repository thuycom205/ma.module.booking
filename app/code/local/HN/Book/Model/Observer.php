<?php

class HN_Book_Model_Observer
{

    /**
     * the funcion is used to hook with event catalog_product_save_after
     * @param Varien_Event_Observer $observer
     * @return Varien_Event_Observer
     * @throws Exception
     */
    public  function prepareProductSave(Varien_Event_Observer $observer)
    {
        $request = $observer->getEvent()->getRequest();
        $product = $observer->getEvent()->getProduct();
        $request_params = Mage::app()->getRequest()->getParams();
        //$bookables = $request->getPost('bookable')
        if (isset($request_params['bookable'])) {

            $bookables = $request_params['bookable'];
            $bookable = $bookables['price'];

            Mage::log($bookable, null, 'booka.log', true);
            Mage::log('product id' . $product->getId(), null, 'booka.log', true);

            $product->setBookableData($bookable);

            $price_rule = array(
                'product_id' => $product->getId(),
                'time_type' => '',
                'time_from' => '',
                'time_to' => '',
                'price' => 0
            );

            //delete the old records
            $priceRules = Mage::getModel('book/pricerule')->getCollection()->addFieldToFilter('product_id' , $product->getId());

            if ($priceRules->getSize() > 0 ) {
                foreach ($priceRules as $old) {
                    $old->delete();

                }
            }

            foreach ($bookable as $key => $value) {
                   // save new
                        if ($this->validatePriceRuleInput($value)) {
                            $value['product_id'] = $product->getId();
                            $model = Mage::getModel('book/pricerule')->setData($value)->save();

                        }

            }
        }

        return $observer;
    }

    private function validatePriceRuleInput($price_rule) {
        if (
             isset($price_rule['time_from'])  && $price_rule['time_from']
            &&  isset($price_rule['time_to'])  && $price_rule['time_to']
            && isset($price_rule['price'])  && $price_rule['price']
        )  {
            return true;
        } else {
            return false;
        }

}

    public function saveBook(Varien_Event_Observer $observer)
    {

        /* @var $orderItem Mage_Sales_Model_Order_Item */
        $orderItem = $observer->getEvent()->getItem();
        if (!$orderItem->getId()) {
            //order not saved in the database
            return $this;
        }

        $warning_message = '';
        $product = $orderItem->getProduct();

        $product_id = $product->getId();
        $start_time = '';

        $end_time = '';

        $format = 'Y-m-d H:i:s';
        $info = '';

        /* $booking status */
// 		there are some defualt of booking status such 

// 		1. not -confirm 0
// 		2.confirm  1 or cancel 2 
// 		2. cofirm 3 
// 		4. complete 4

        $booking_status = 0;

        if ($product && $product->getTypeId() != 'book') {
            return;
        }

        if (!$product) {
            $product = Mage::getModel('catalog/product')
                ->setStoreId($orderItem->getOrder()->getStoreId())
                ->load($orderItem->getProductId());
        }

        $orderId = $orderItem->getOrderId();

        /* @var $order Mage_Sales_Mode_Order */

        /* boolean */
        $have_enough_resource = true;

        $order = Mage::getModel('sales/order')->load($orderId);
        /* @var $order Mage_Sales_Model_Order */
        $order->getIncrementId();

        $order->getCustomerId();

        $options = $orderItem->getProductOptions();

        /* get the mapping of custom option in the module configuration */

        $adult_amout = '';
        $child_amount = '';
        $estimate_arrive_time = '';


        /* adult amout */
        $adult_amout_label = Mage::getStoreConfig('book/option/adult_amount');
        /* adult amout */
        $child_amount_label = Mage::getStoreConfig('book/option/child_amount');
        /* adult amout */
        $estimate_arrive_time_label = Mage::getStoreConfig('book/option/estimate_arrive_time');

        /* loop through the options */
        if ($options) {
            foreach ($options as $option) {

                if ($option['label'] == $adult_amout_label) {
                    $adult_amout = $option['value'];

                }

                //
                if ($option['label'] == $child_amount_label) {
                    $child_amount = $option['value'];

                }

                //
                if ($option['label'] == $estimate_arrive_time_label) {
                    $estimate_arrive_time = $option['value'];

                }
            }
        }

        Mage::log($options, null, 'booka.log', true);

        /* @var $adds array()  additional option */
        $adds = $orderItem->getProductOptionByCode('additional_options');//additional_options();

        if ($adds) {

            foreach ($adds as $key => $option) {

                if ($option['label'] == 'StartTime') {
                    $start_time = $option['value'];
                    $the_date = new DateTime($start_time);
                    $start_time = $the_date->format($format);
                }

                if ($option['label'] == 'EndTime') {
                    $end_time = $option['value'];

                    $the_date = new DateTime($end_time);
                    $end_time = $the_date->format($format);
                }

            }

            $info = serialize($adds);

        }

        $have_enough_resource = $this->haveEnoughResource($product_id, $start_time, $end_time);

        if (!$have_enough_resource) {
            $warning_message = "There is not enough resource to create the booking";

            $force_create_booking = Mage::getStoreConfig('book/general/force_create_booking');

            if ($force_create_booking != 'yes') {

                //quit without creat booking

                return;
            }
        }


        Mage::log($adds, null, 'booka.log', true);
        Mage::log('product type id ' . $product->getTypeId(), null, 'booka.log', true);
        if ($product->getTypeId() == 'book') {
            //if ($times = $orderItem->getProductOptionByCode('booktimes')) {

            $bookData = array(
                'title' => 'order ' . $order->getIncrementId(),
                'order_id' => $order->getId(),
                'order_item_id' => $orderItem->getId(),
                'product_id' => $product->getId(),
                'product_name' => $product->getName(),
                'status' => $booking_status,
                'customer_id' => $order->getCustomerId(),
                'customer_name' =>$order->getCustomerName(),
                'start_time' => $start_time,
                'end_time' => $end_time,
                'info' => $info,
                'adult_amount' => 0,
                'child_amount' => 0,
                'estimate_arrive_time' => ''
            );

            if ($adult_amout) $bookData['adult_amount'] = $adult_amout;
            if ($child_amount) $bookData['child_amount'] = $child_amount;
            if ($estimate_arrive_time) $bookData['estimate_arrive_time'] = $estimate_arrive_time;


            $book = Mage::getModel('book/book')->setData($bookData)->save();

            Mage::dispatchEvent('booking_generate', array('book' => $book, 'book_id' => $book->getId(), 'order_item' => $orderItem));

            //draft end
            //}
        }

        return $observer;
    }

    public function haveEnoughResource($product_id, $start_time, $end_time)
    {
        /**
         * process the resource that belongs to the product
         * 1.is_mandatory
         * 2.is_assign_automatically
         */
        $have_enough_resource = true;

        $associated_resources = Mage::getModel('book/bresource')->getCollection()->getResourceByProductId($product_id);

        if ($associated_resources->getSize() > 0) {
            foreach ($associated_resources as $resourceItem) {

                /* @var $resourceItem HN_Book_Model_BResource */

                $is_mandatory = $resourceItem->getIsMandatory();

                $slots = $resourceItem->getCanServerHowmany();

                $is_auto_assign = $resourceItem->getIsAutoAssign();

                /* get the booking item in a time period*/
                $reservedItems = Mage::getModel('book/bresource')->getCollection()->getReservingItems($resourceItem->getId(), $start_time, $end_time);

                $reservedItemsAmount = $reservedItems->getSize();

                if ($slots == $reservedItemsAmount) {
                    $warning_message = Mage::helper('book')->__('The resource is out stocked');

                }

                if ($is_mandatory && ($reservedItems->getStatus() != 'available') || $reservedItemsAmount < $slots || $slots == 0) {
                    $have_enough_resource = false;
                }
                if ($is_auto_assign == 'yes' && $reservedItemsAmount < $slots && $reservedItems->getStatus() == 'available') {

                    //create a booking resource id releation record
                    Mage::getModel('book/brrelation')->setData(array('book_id' => $book_id, 'resource_id' => $reservedItems))->save();
                }

            }

        }

        return $have_enough_resource;
    }

    public function assignResource($product_id, $from, $to, $book_id)
    {
        $associated_resources = Mage::getModel('book/bresource')->getCollection()->getResourceByProductId($product_id);
        if ($associated_resources->getSize() > 0) {
            foreach ($associated_resources as $resourceItem) {

                /* @var $resourceItem HN_Book_Model_BResource */

                $is_mandatory = $resourceItem->getIsMandatory();

                $slots = $resourceItem->getCanServerHowmany();

                $is_auto_assign = $resourceItem->getIsAutoAssign();

                /* get the booking item in a time period*/
                $reservedItems = Mage::getModel('book/bresource')->getCollection()->getReservingItems($resourceItem->getId(), $start_time, $end_time);

                $reservedItemsAmount = $reservedItems->getSize();


                if ($is_auto_assign == 'yes' && $reservedItemsAmount < $slots && $reservedItems->getStatus() == 'available' && $slots > 0) {

                    //create a booking resource id releation record
                    Mage::getModel('book/brrelation')->setData(array('book_id' => $book_id, 'resource_id' => $reservedItems->getId()))->save();
                }

            }

        }
    }

    public function saveDownloadableOrderItem($observer)
    {
        $orderItem = $observer->getEvent()->getItem();
        if (!$orderItem->getId()) {
            //order not saved in the database
            return $this;
        }
        $product = $orderItem->getProduct();
        if ($product && $product->getTypeId() != Mage_Downloadable_Model_Product_Type::TYPE_DOWNLOADABLE) {
            return $this;
        }
        if (Mage::getModel('downloadable/link_purchased')->load($orderItem->getId(), 'order_item_id')->getId()) {
            return $this;
        }
        if (!$product) {
            $product = Mage::getModel('catalog/product')
                ->setStoreId($orderItem->getOrder()->getStoreId())
                ->load($orderItem->getProductId());
        }
        if ($product->getTypeId() == Mage_Downloadable_Model_Product_Type::TYPE_DOWNLOADABLE) {
            $links = $product->getTypeInstance(true)->getLinks($product);
            if ($linkIds = $orderItem->getProductOptionByCode('links')) {
                $linkPurchased = Mage::getModel('downloadable/link_purchased');
                Mage::helper('core')->copyFieldset(
                    'downloadable_sales_copy_order',
                    'to_downloadable',
                    $orderItem->getOrder(),
                    $linkPurchased
                );
                Mage::helper('core')->copyFieldset(
                    'downloadable_sales_copy_order_item',
                    'to_downloadable',
                    $orderItem,
                    $linkPurchased
                );
                $linkSectionTitle = (
                $product->getLinksTitle() ?
                    $product->getLinksTitle() : Mage::getStoreConfig(Mage_Downloadable_Model_Link::XML_PATH_LINKS_TITLE)
                );
                $linkPurchased->setLinkSectionTitle($linkSectionTitle)
                    ->save();
                foreach ($linkIds as $linkId) {
                    if (isset($links[$linkId])) {
                        $linkPurchasedItem = Mage::getModel('downloadable/link_purchased_item')
                            ->setPurchasedId($linkPurchased->getId())
                            ->setOrderItemId($orderItem->getId());

                        Mage::helper('core')->copyFieldset(
                            'downloadable_sales_copy_link',
                            'to_purchased',
                            $links[$linkId],
                            $linkPurchasedItem
                        );
                        $linkHash = strtr(base64_encode(microtime() . $linkPurchased->getId() . $orderItem->getId()
                            . $product->getId()), '+/=', '-_,');
                        $numberOfDownloads = $links[$linkId]->getNumberOfDownloads() * $orderItem->getQtyOrdered();
                        $linkPurchasedItem->setLinkHash($linkHash)
                            ->setNumberOfDownloadsBought($numberOfDownloads)
                            ->setStatus(Mage_Downloadable_Model_Link_Purchased_Item::LINK_STATUS_PENDING)
                            ->setCreatedAt($orderItem->getCreatedAt())
                            ->setUpdatedAt($orderItem->getUpdatedAt())
                            ->save();
                    }
                }
            }
        }

        return $this;
    }

    /**
     * Set status of book
     * @param Varien_Event_Observer $observer
     */
    public function setBookStatus(Varien_Event_Observer $observer)
    {
        $order = $observer->getEvent()->getOrder();

        if (!$order->getId()) {
            //order not saved in the database
            return $this;
        }

        /* @var $order Mage_Sales_Model_Order */
// 		$status = '';
// 		$linkStatuses = array(
// 				'pending'         => Mage_Downloadable_Model_Link_Purchased_Item::LINK_STATUS_PENDING,
// 				'expired'         => Mage_Downloadable_Model_Link_Purchased_Item::LINK_STATUS_EXPIRED,
// 				'avail'           => Mage_Downloadable_Model_Link_Purchased_Item::LINK_STATUS_AVAILABLE,
// 				'payment_pending' => Mage_Downloadable_Model_Link_Purchased_Item::LINK_STATUS_PENDING_PAYMENT,
// 				'payment_review'  => Mage_Downloadable_Model_Link_Purchased_Item::LINK_STATUS_PAYMENT_REVIEW
// 		);

    }

    public function initOptionRenderer(Varien_Event_Observer $observer)
    {
        $block = $observer->getBlock();
        $block->addOptionsRenderCfg('downloadable', 'downloadable/catalog_product_configuration');
        return $this;
    }

    public function catalogProductLoadAfter($observer)
    {
        $action = Mage::app()->getFrontController()->getAction();
        if (is_object($action)) {
            if ($action->getFullActionName() == 'checkout_cart_add') {
                // assuming you are posting your custom form values in an array called extra_options...
                if ($options = $action->getRequest()->getParam('bookable_option')) {

                    $product = $observer->getProduct();
                    $options = $action->getRequest()->getParam('bookable_option');
                    // $options= array('balance'=>'20');
                    // add to the additional options array
                    $additionalOptions = array();
                    if ($additionalOption = $product->getCustomOption('additional_options')) {
                        $additionalOptions = ( array )unserialize($additionalOption->getValue());
                    }
                    foreach ($options as $key => $value) {
                        //if ($key = 'StartTime') $key = 'Start Time';
                        //if ($key = 'EndTime') $key = 'End Time';
                        $additionalOptions [] = array(
                            'label' => $key,
                            'value' => $value
                        );
                    }
                    // add the additional options array with the option code additional_options
                    $observer->getProduct()->addCustomOption('additional_options', serialize($additionalOptions));
                }
            }
        }
    }

    public function salesConvertQuoteItemToOrderItem(Varien_Event_Observer $observer)
    {
        $quoteItem = $observer->getItem();
        if ($additionalOptions = $quoteItem->getOptionByCode('additional_options')) {
            $orderItem = $observer->getOrderItem();
            $options = $orderItem->getProductOptions();
            $options ['additional_options'] = unserialize($additionalOptions->getValue());
            $orderItem->setProductOptions($options);
        }
    }

    public function checkoutCartProductAddAfter(Varien_Event_Observer $observer)
    {
        $action = Mage::app()->getFrontController()->getAction();
        if ($action->getFullActionName() == 'sales_order_reorder') {
            $item = $observer->getQuoteItem();
            $buyInfo = $item->getBuyRequest();
            if ($options = $buyInfo->getExtraOptions()) {
                $additionalOptions = array();
                if ($additionalOption = $item->getOptionByCode('additional_options')) {
                    $additionalOptions = ( array )unserialize($additionalOption->getValue());
                }
                foreach ($options as $key => $value) {
                    $additionalOptions [] = array(
                        'label' => $key,
                        'value' => $value
                    );
                }
                $item->addOption(array(
                    'code' => 'additional_options',
                    'value' => serialize($additionalOptions)
                ));
            }
        }
    }


    /**
     * Assign resource to booking and show notification
     * @param Varien_Event_Observer $observer
     */
    public function bookingAddAfter(Varien_Event_Observer $observer)
    {

        $id = $observer->getEvent()->getData('book_id');

        //assign resource to book
        //get available resource
        $model = Mage::getModel('book/bresource')->getAvailableResources();

    }
}