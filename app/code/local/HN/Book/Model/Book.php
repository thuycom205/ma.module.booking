<?php
class HN_Book_Model_Book extends Mage_Core_Model_Abstract {
	
	const STATUS_COMPLETE = 'complete'; //3
	const STATUS_PAID = 'paid';
	const SATUS_CONFIRM = 'confirm';//2
	const  STATUS_UNPAID = 'unpaid';
	
	const STATUS_CANCELLED = 'cancelled';//1
	const STATUS_PENDING_CONFIRMATION   = 'pending-confirmation'; //0
	
	/**
	 * Initialize resource model
	 *
	 */
	protected function _construct()
	{
		$this->_init('book/book');
		parent::_construct();
	}
}