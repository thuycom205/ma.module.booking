<?php
class HN_Book_Model_Resource extends Mage_Core_Model_Abstract {
	
	const STATUS_COMPLETE = 'complete';
	const STATUS_PAID = 'paid';
	const SATUS_CONFIRM = 'confirm';
	const  STATUS_UNPAID = 'unpaid';
	
	const STATUS_CANCELLED = 'cancelled';
	const STATUS_PENDING_CONFIRMATION   = 'pending-confirmation';
	
	/**
	 * Initialize resource model
	 *
	 */
	protected function _construct()
	{
		$this->_init('book/resource');
		parent::_construct();
	}
}