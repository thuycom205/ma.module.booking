<?php
class HN_Book_Adminhtml_CalendarController extends Mage_Adminhtml_Controller_Action {
	public function indexAction() {
		$this->_title($this->__('Calendar - Booking'));
	
		$this->loadLayout ()->_setActiveMenu ( 'book/calendar' );
	
		$this->_addContent($this->getLayout()->createBlock('book/adminhtml_book_calendar'));
	
		$this->renderLayout ();
	}

    public function eventsAction() {
        $output = array();
        $books = Mage::getModel('book/book')->getCollection();
        if ($books->getSize()) {
            foreach ($books as $book) {
                $startDate = new DateTime($book->getData('start_time'));
                $startTime = $startDate->format('Y-m-d');

                $endDate = new DateTime($book->getData('end_time'));
                $endTime = $endDate->format('Y-m-d');


                $event1 = array (
                    'title' =>$book->getData('product_name') . ' ' . $book->getData('title'),
                    'url' => $this->getUrl('book_admin/adminhtml_book/index'),
                    'start' =>  $startTime,
                    'end' =>  $endTime,
                );

                $output[] = $event1;
            }
        }
	echo json_encode($output);

    }
}