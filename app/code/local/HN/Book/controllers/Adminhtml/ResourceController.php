<?php
class HN_Book_Adminhtml_ResourceController extends Mage_Adminhtml_Controller_Action {
	public function indexAction() {
		$this->_title ( $this->__ ( 'Resource Management | Booking' ) );
		
		$this->loadLayout ()->_setActiveMenu ( 'book/resource' );
		
		$this->_addContent ( $this->getLayout ()->createBlock ( 'book/adminhtml_resource_resource' ) );
		
		$this->renderLayout ();
	}
	

	/**
	 * create new resource for booking
	 * 
	 */
	public function newAction()
	{
		$id = $this->getRequest()->getParam('id');
		$model = Mage::getModel('book/bresource')->load($id);
	
		$data = Mage::getSingleton('adminhtml/session')->getFormData(true);
		if (!empty($data)) {
			$model->setData($data);
		}
	
		Mage::register('bresource_data', $model);
	
		$this->loadLayout();
		$this->_setActiveMenu('book/resource');
		$this->_title('Add new resource for booking');
	
		$this->getLayout()->getBlock('head')->setCanLoadExtJs(true);
	
		$this
		->_addContent($this->getLayout()->createBlock('book/adminhtml_resource_edit'))
		->_addLeft($this->getLayout()->createBlock('book/adminhtml_resource_edit_tabs'))
		;
		$this->getLayout()->getBlock('head')->setCanLoadTinyMce(true);
		$this->renderLayout();
	}
	
	public function saveajaxAction() {
		$params = $this->getRequest()->getParams();
		
		$output = array();
		
		if (isset($params['is_ajax']) && $params['is_ajax'] =='yes') {
			
			$model = Mage::getModel('book/bresource');

			$model
			->setData($params)
			->setId($this->getRequest()->getParam('id'))
			;
				
				
			try {
			
				$model->save();
				
				$output['result'] = 'success';
				$output['message'] = __('The resource is successfully saved');
				$output['data'] =  $model->getData();
				
			} catch (Exception $e) {
				$output['result'] = 'error';
				$output['message'] = __('There is error occurs');
				$output['data'] =  '';
			}
			$data = json_encode ( $output );
			$this->getResponse ()->setBody ( $data );
			return;
		}
	}
	
	
	public function saveAction() {
		
		if ($data = $this->getRequest()->getPost()) {
			
			$model = Mage::getModel('book/bresource');
			
			// @TODO need validation
			
			$model
			->setData($data)
			->setId($this->getRequest()->getParam('id'))
			;
			
			
			try {
				
				$model->save();
				Mage::getSingleton('adminhtml/session')->addSuccess(
						Mage::helper('book')->__('Resource was successfully saved')
				);
				
				if ($this->getRequest()->getParam('back')) {
					$this->_redirect('*/*/edit', array('id' => $model->getId()));
					return;
				}
				$this->_redirect('*/*/');
				return;
				
			 } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setFormData($data);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            }
			
		}
		
		Mage::getSingleton('adminhtml/session')->addError(Mage::helper('blog')->__('Unable to find post to save'));
		$this->_redirect('*/*/');
	}
	
	
	public function deleteAction() {
		$Ids = $this->getRequest ()->getParam ( 'id' );
		if (! is_array ( $Ids )) {
			Mage::getSingleton ( 'adminhtml/session' )->addError ( Mage::helper ( 'adminhtml' )->__ ( 'Please select item(s)' ) );
		} else {
			try {
				foreach ( $Ids as $id ) {
					$model = Mage::getModel ( 'advancedreview/mail' )->load ( $id );
					$model->delete ();
				}
				Mage::getSingleton ( 'adminhtml/session' )->addSuccess ( Mage::helper ( 'adminhtml' )->__ ( 'Total of %d record(s) were successfully deleted', count ( $Ids ) ) );
			} catch ( Exception $e ) {
				Mage::getSingleton ( 'adminhtml/session' )->addError ( $e->getMessage () );
			}
		}
		$this->_redirect ( '*/*/index' );
	}
	
	public function editAction()
	{
		$id = $this->getRequest()->getParam('id');
		$model = Mage::getModel('book/bresource')->load($id);
	
		if ($model->getId() || $id == 0) {
			$data = Mage::getSingleton('adminhtml/session')->getFormData(true);
			if (!empty($data)) {
				$model->setData($data);
			}
	
			Mage::register('bresource_data', $model);
	
			$this->loadLayout();
			$this->_setActiveMenu('book/resource');
			$this->_title('Edit Resource');
	
			$this->getLayout()->getBlock('head')->setCanLoadExtJs(true);
	
			$this
			->_addContent($this->getLayout()->createBlock('book/adminhtml_resource_edit'))
			->_addLeft($this->getLayout()->createBlock('book/adminhtml_resource_edit_tabs'))
			;
	
			$this->getLayout()->getBlock('head')->setCanLoadTinyMce(true);
	
			$this->renderLayout();
		} else {
			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('book')->__('Resource does not exist'));
			$this->_redirect('*/*/');
		}
	}
	
	
	public function sendAction() {
		$Ids = $this->getRequest ()->getParam ( 'id' );
		if (! is_array ( $Ids )) {
			Mage::getSingleton ( 'adminhtml/session' )->addError ( Mage::helper ( 'adminhtml' )->__ ( 'Please select item(s)' ) );
		} else {
			try {
				foreach ( $Ids as $id ) {
					$model = Mage::getModel ( 'advancedreview/mail' )->load ( $id );
					$model->send ();
				}
				Mage::getSingleton ( 'adminhtml/session' )->addSuccess ( Mage::helper ( 'adminhtml' )->__ ( 'Total of %d record(s) were successfully send', count ( $Ids ) ) );
			} catch ( Exception $e ) {
				Mage::getSingleton ( 'adminhtml/session' )->addError ( $e->getMessage () );
			}
		}
		$this->_redirect ( '*/*/index' );
	}
	public function cancelAction() {
		$Ids = $this->getRequest ()->getParam ( 'id' );
		if (! is_array ( $Ids )) {
			Mage::getSingleton ( 'adminhtml/session' )->addError ( Mage::helper ( 'adminhtml' )->__ ( 'Please select item(s)' ) );
		} else {
			try {
				foreach ( $Ids as $id ) {
					$model = Mage::getModel ( 'advancedreview/mail' )->load ( $id );
					$model->setStatus ( 3 )->save ();
				}
				Mage::getSingleton ( 'adminhtml/session' )->addSuccess ( Mage::helper ( 'adminhtml' )->__ ( 'Total of %d record(s) were successfully send', count ( $Ids ) ) );
			} catch ( Exception $e ) {
				Mage::getSingleton ( 'adminhtml/session' )->addError ( $e->getMessage () );
			}
		}
		$this->_redirect ( '*/*/index' );
	}
	
}