<?php
/**
 * Created by Magenest
 * User: Luu Thanh Thuy
 * Date: 26/02/2016
 * Time: 16:23
 */

class HN_Book_Adminhtml_NotificationController  extends Mage_Adminhtml_Controller_Action
{

    public function IndexAction() {
        $this->_title ( $this->__ ( 'Send Notification Email | Booking' ) );


        $this->loadLayout ()->_setActiveMenu ( 'book/global' );

        $this->_addContent($this->getLayout()->createBlock('book/adminhtml_book_notification'));

        $this->renderLayout ();
    }

    public function sendAction() {
        $params = $this->getRequest()->getParams();

        $product_id = $params['product_id'];

        $status = $params['status'];


        $senderEmail = Mage::getStoreConfig('trans_email/ident_general/email');
        $senderName = Mage::getStoreConfig('trans_email/ident_general/name');
        /**
         *
         * @var $emailTemplate Mage_Core_Model_Email_Template
         */
        $emailTemplate = Mage::getModel ( 'core/email_template' );
        $emailTemplate->setSenderName ( $senderName );
        $emailTemplate->setSenderEmail ( $senderEmail );
        // $emailTemplate = new Mage_Core_Model_Email_Template();
//		$bcc = array (
//				'0' => 'joe@example.com',
//				'1' => 'doe@example.com'
//		);
//		$emailTemplate->addBcc ( $bcc );
        $emailTemplate->setTemplateSubject ( $params['subject'] );
        $emailTemplate->setTemplateText (  $params['content'] );//content

        //Filter the book that match the condition of the
        $collections = Mage::getModel('book/book')->getCollection()->addFieldToFilter('product_id')
            ->addFieldToFilter('status',$status);

        if ($collections->getSize() ) {
            foreach ($collections as $booking) {
                $customer_id  = $booking->getData('customer_id');

                /** @var  $customer Mage_Customer_Model_Customer */
                $customer = Mage::getModel('customer/customer')->load($customer_id);
                $recipient_email = $customer->getEmail();

                $recipient_name = $customer->getFirstname() . ' '. $customer->getLastname();
                if (!$customer->getId()) {
                    /** @var  $order Mage_Sales_Model_Order */
                    $order = Mage::getModel('sales/order')->load($booking->getData('order_id'));

                    $recipient_email = $order->getCustomerEmail();
                    $recipient_name = $order->getCustomerName();

                }

                $emailTemplate->send ( $recipient_email, $recipient_name );

                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('adminhtml')->__('Email(s) was successfully sent')
                );

            }

        } else {
            Mage::getSingleton('adminhtml/session')->addWarning(
                Mage::helper('adminhtml')->__('There is no email that matching your conditions')
            );

        }

        $this->_redirect('*/*/');
    }

}