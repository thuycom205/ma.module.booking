<?php
/**
 * Created by Magenest
 * User: Luu Thanh Thuy
 * Date: 25/02/2016
 * Time: 11:07
 */

class HN_Book_Adminhtml_GlobalController   extends Mage_Adminhtml_Controller_Action
{
    public function indexAction() {
      $this->_title ( $this->__ ( 'Global Availibility Rule | Booking' ) );

        $this->loadLayout ()->_setActiveMenu ( 'book/global' );


       $this->renderLayout ();
    }

    public function saveAction() {

        $params = $this->getRequest()->getParams('ga');

        if ($params){
            foreach ($params as $ga) {
                if ($ga)
                foreach ($ga as $data_ga ){
                    if ($this->validateInput($data_ga)) {
                        $startDate = new DateTime($data_ga['time_from']);
                        $data_ga['time_from'] = $startDate->format('Y-m-d H:i:s');

                        $endDate = new DateTime($data_ga['time_to']);
                        $data_ga['time_to'] = $endDate->format('Y-m-d H:i:s');
                        Mage::getModel('book/avarule')->setData($data_ga)->save();

                    }
                }
            }

        }
        $this->_redirect('*/*/');


    }

    private function validateInput($input) {
        $isValidate = true;
        if (!isset($input['time_type'])  || !isset($input['time_from']) || !isset($input['time_to']) || !$input['time_from'] )
            $isValidate= false;
        return $isValidate;

}

}