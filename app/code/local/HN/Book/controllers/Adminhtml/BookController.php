<?php
class HN_Book_Adminhtml_BookController extends Mage_Adminhtml_Controller_Action
{
    /**
     * init layout and set active for current menu
     *
     * @return HN_Advancedreview_Adminhtml_AdvancedreviewController
     */
    protected function _initAction()
    {
        $this->loadLayout()
            ->_setActiveMenu('book/all')
            ->_addBreadcrumb(
                Mage::helper('adminhtml')->__('Bookings'),
                Mage::helper('adminhtml')->__('All Bookings')
            );
        return $this;
    }
 
    /**
     * index action
     */
    public function indexAction()
    {
        $this->_title($this->__('All bookings'));
		
		$this->loadLayout ()->_setActiveMenu ( 'book/all' );
		
		$this->_addContent($this->getLayout()->createBlock('book/adminhtml_book_book'));
		
		$this->renderLayout ();
    }

    /**
     * view and edit item action
     */
    public function editAction()
    {
        $id     = $this->getRequest()->getParam('id');
        $model  = Mage::getModel('book/book')->load($id);

         if ($model->getId() || $id == 0) {
            $data = Mage::getSingleton('adminhtml/session')->getFormData(true);
            if (!empty($data)) {
                $model->setData($data);
            }

            Mage::register('form_data', $model);

            $this->loadLayout();
            $this->_setActiveMenu('book/item');
            $this->_title('Edit');

            $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);
            $this->getLayout()->getBlock('head')->setCanLoadTinyMce(true);

            $this->renderLayout();
        } else {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('book')->__('Booking does not exist'));
            $this->_redirect('*/*/');
        }
    }
 
    public function newAction()
    {
        $this->_forward('edit');
    }
 
    /**
     * save item action
     */
    public function saveAction()
    {
        if ($data = $this->getRequest()->getPost()) {
            if (isset($_FILES['filename']['name']) && $_FILES['filename']['name'] != '') {
                try {
                    /* Starting upload */    
                    $uploader = new Varien_File_Uploader('filename');
                    
                    // Any extention would work
                       $uploader->setAllowedExtensions(array('jpg','jpeg','gif','png'));
                    $uploader->setAllowRenameFiles(false);
                    
                    // Set the file upload mode 
                    // false -> get the file directly in the specified folder
                    // true -> get the file in the product like folders 
                    //    (file.jpg will go in something like /media/f/i/file.jpg)
                    $uploader->setFilesDispersion(false);
                            
                    // We set media as the upload dir
                    $path = Mage::getBaseDir('media') . DS ;
                    $result = $uploader->save($path, $_FILES['filename']['name'] );
                    $data['filename'] = $result['file'];
                } catch (Exception $e) {
                    $data['filename'] = $_FILES['filename']['name'];
                }
            }
              
            $model = Mage::getModel('advancedreview/advancedreview');        
            $model->setData($data)
                ->setId($this->getRequest()->getParam('id'));
            
            try {
                if ($model->getCreatedTime == NULL || $model->getUpdateTime() == NULL) {
                    $model->setCreatedTime(now())
                        ->setUpdateTime(now());
                } else {
                    $model->setUpdateTime(now());
                }
                $model->save();
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('advancedreview')->__('Item was successfully saved')
                );
                Mage::getSingleton('adminhtml/session')->setFormData(false);

                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', array('id' => $model->getId()));
                    return;
                }
                $this->_redirect('*/*/');
                return;
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setFormData($data);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            }
        }
        Mage::getSingleton('adminhtml/session')->addError(
            Mage::helper('advancedreview')->__('Unable to find item to save')
        );
        $this->_redirect('*/*/');
    }
 
    /**
     * delete item action
     */
    public function deleteAction()
    {
        if ($this->getRequest()->getParam('id') > 0) {
            try {
                $model = Mage::getModel('advancedreview/advancedreview');
                $model->setId($this->getRequest()->getParam('id'))
                    ->delete();
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('adminhtml')->__('Item was successfully deleted')
                );
                $this->_redirect('*/*/');
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
            }
        }
        $this->_redirect('*/*/');
    }

    /**
     * mass delete item(s) action
     */
    public function massDeleteAction()
    {
        $advancedreviewIds = $this->getRequest()->getParam('advancedreview');
        if (!is_array($advancedreviewIds)) {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')->__('Please select item(s)'));
        } else {
            try {
                foreach ($advancedreviewIds as $advancedreviewId) {
                    $advancedreview = Mage::getModel('advancedreview/advancedreview')->load($advancedreviewId);
                    $advancedreview->delete();
                }
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('adminhtml')->__('Total of %d record(s) were successfully deleted',
                    count($advancedreviewIds))
                );
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/index');
    }
    
    /**
     * mass change status for item(s) action
     */
    public function massStatusAction()
    {
        $advancedreviewIds = $this->getRequest()->getParam('advancedreview');
        if (!is_array($advancedreviewIds)) {
            Mage::getSingleton('adminhtml/session')->addError($this->__('Please select item(s)'));
        } else {
            try {
                foreach ($advancedreviewIds as $advancedreviewId) {
                    Mage::getSingleton('advancedreview/advancedreview')
                        ->load($advancedreviewId)
                        ->setStatus($this->getRequest()->getParam('status'))
                        ->setIsMassupdate(true)
                        ->save();
                }
                $this->_getSession()->addSuccess(
                    $this->__('Total of %d record(s) were successfully updated', count($advancedreviewIds))
                );
            } catch (Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/index');
    }

    /**
     * export grid item to CSV type
     */
    public function exportCsvAction()
    {
        $fileName   = 'advancedreview.csv';
        $content    = $this->getLayout()
                           ->createBlock('advancedreview/adminhtml_advancedreview_grid')
                           ->getCsv();
        $this->_prepareDownloadResponse($fileName, $content);
    }

    /**
     * export grid item to XML type
     */
    public function exportXmlAction()
    {
        $fileName   = 'advancedreview.xml';
        $content    = $this->getLayout()
                           ->createBlock('advancedreview/adminhtml_advancedreview_grid')
                           ->getXml();
        $this->_prepareDownloadResponse($fileName, $content);
    }
    
    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('advancedreview');
    }
    
    
    /**
     * assign resource to  product
     * 
     */
    
    public function assignresourceAction()  {
    	$book_id = $this->getRequest()->getParam('id');
    	if (!$book_id) {
    		return ;
    	}
    	
    	/* @var $book HN_Book_Model_Book */
    	$book = Mage::getModel('book/book')->load($book_id);
    	
    	//render the grid of resource so user can assign it
    	
    	$this->_title($this->__('Select a resource to assign it to booking'));
    	
    	$this->loadLayout ()->_setActiveMenu ( 'book/all' );
    	
    	//add the blog name assign to booking
    	$this->_addContent($this->getLayout()->createBlock('core/template' , 'book.assign' ,array('template' =>'hn/book/catalog/assign_resource_button.phtml')));
    	 
    	$this->_addContent($this->getLayout()->createBlock('book/adminhtml_resource_grid'));
    	
    	$this->renderLayout ();
    	
    }
    
    
    public function saveresourcetobookAction() {
    	$params = $this->getRequest()->getParams();
    	
    	//var_dump($params);
    	
    	if (!isset($params['book_id'])) {
    		return;
    	}
    	
    	//////////
    	$book_id = $params['book_id'];
    	
    	$resource_ids = $params['resource_ids'];
    	
    	if (!$resource_ids) {
    		return;
    	}
    	
    	$resource_id_arr  = explode(',', $resource_ids);
    	

    	
    	if ($resource_id_arr) {
    		foreach ($resource_id_arr as $resource_id) {

                /* @$model HN_Book_Model_Brrelation */
                $collects = Mage::getModel('book/brrelation')->getCollection()->addFieldToFilter('book_id',$book_id)
                    ->addFieldToFilter('resource_id',$resource_id);

                if ( !$collects->getSize() > 0)  {
                    Mage::getModel('book/brrelation')->setData(array('book_id' => $book_id, 'resource_id' =>$resource_id)) ->save();
                }
    				
    		}
    	} 

        $this->_redirect('book_admin/adminhtml_book/index');
    }
    
    
    public function unassignAction() {
    	
    	$id = $this->getRequest()->getParam('id');
    	//resource id   rs_id
    	$resource_id = $this->getRequest()->getParam('rs_id');

        $collects = Mage::getModel('book/brrelation')->getCollection()->addFieldToFilter('book_id',$id)
            ->addFieldToFilter('resource_id',$resource_id);

        if ($collects->getSize() > 0) {

            foreach ($collects as $relation) {
                $relation->delete();
            }
        }

        $this->_redirect('book_admin/adminhtml_book/index');
    }
}