<?php
class HN_Book_Helper_Catalog_Product_Configuration extends Mage_Core_Helper_Abstract
    implements Mage_Catalog_Helper_Product_Configuration_Interface {
	public function getTimes(Mage_Catalog_Model_Product_Configuration_Item_Interface $item)
	{
		$product = $item->getProduct();
		$itemTimes = array();
		$times = $item->getOptionByCode('bookable_times');
		
		if ($times) {
			$value = $times->getValue();
			
			if (strpos($value, ',')) {
				$itemTimes = explode(',', $value);
			} else {
				$itemTimes[] = $value;
			}
			
			return $itemTimes;
			
// 			$productLinks = $product->getTypeInstance(true)
// 			->getLinks($product);
// 			foreach (explode(',', $linkIds->getValue()) as $linkId) {
// 				if (isset($productLinks[$linkId])) {
// 					$itemLinks[] = $productLinks[$linkId];
// 				}
// 			}
		}
		return $itemTimes;
	}
	
	public function getOptions(Mage_Catalog_Model_Product_Configuration_Item_Interface $item)
	{
		$options = Mage::helper('catalog/product_configuration')->getOptions($item);
	
		$links = $this->getTimes($item);
		if ($links) {
			$linksOption = array(
					'label' =>$item->getProduct()->getName(),
					'value' => array()
			);
			foreach ($links as $link) {
				$linksOption['value'][] = $link;
			}
			$options[] = $linksOption;
		}
	
		return $options;
	}
}