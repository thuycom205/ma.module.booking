<?php
//adminhtml/default/default/template/sales/order/view/items/renderer/default.phtml
/* Mage_Adminhtml_Block_Sales_Order_View_Items_Renderer_Default */
/* @var $parent Mage_Catalog_Block_Product_View */
class HN_Book_Block_Catalog_Product_Bookdate extends Mage_Catalog_Block_Product_Abstract {
	public function __construct()
	{
		parent::__construct();
		//        $this->setSkipGenerateContent(true);
		$this->setTemplate('hn/book/catalog/product/bookdate.phtml');
	}

    /**
     * @param int $month
     */
    public function getUnAvailableDays($month= null) {
      return  Mage::getModel('book/avarule')->getCollection()->addFieldToFilter('bookable-status' ,'0');
    }
	protected function _toHtml() {
		//echo "date picker";
	    return parent::_toHtml();
	}
}