<?php
class HN_Book_Block_Checkout_Cart_Item_Renderer extends Mage_Checkout_Block_Cart_Item_Renderer {
	public function getBookTimes() {
		/* @var $item */
		$item = $this->getItem();
		return Mage::helper('book/catalog_product_configuration')->getTimes($item);
	}
}