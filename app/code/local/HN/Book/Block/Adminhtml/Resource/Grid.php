<?php
//HN_Book_Block_Adminhtml
class HN_Book_Block_Adminhtml_Resource_Grid extends Mage_Adminhtml_Block_Widget_Grid {
	public function __construct() {
		parent::__construct ();
		$this->setId ( 'ticketGrid' );
		$this->setDefaultSort ( 'id' );
		$this->setDefaultDir ( 'ASC' );
		$this->setSaveParametersInSession ( true );
	}
	
	protected function _prepareCollection() {
		$collection = Mage::getModel ( 'book/bresource' )->getCollection ();
		$this->setCollection ( $collection );
		return parent::_prepareCollection ();
	}
	
	protected function _prepareColumns() {
		$this->addColumn ( 'id', array (
				'header' => Mage::helper ( 'book' )->__ ( 'ID' ),
				'align' => 'right',
				'width' => '50px',
				'index' => 'id' 
		) );
		
		$this->addColumn ( 'name', array (
				'header' => Mage::helper ( 'book' )->__ ( 'Name' ),
				'align' => 'right',
				'width' => '50px',
				'index' => 'name' 
		) );
		
		$this->addColumn ( 'description', array (
				'header' => Mage::helper ( 'book' )->__ ( 'Description' ),
				'align' => 'right',
				'width' => '50px',
				'index' => 'description' 
		) );
		
		$this->addColumn ( 'priority', array (
				'header' => Mage::helper ( 'book' )->__ ( 'Priority' ),
				'align' => 'right',
				'width' => '50px',
				'index' => 'priority' 
		) );
		
		$this->addColumn ( 'can_server_howmany', array (
				'header' => Mage::helper ( 'book' )->__ ( 'Can serve' ),
				'align' => 'right',
				'width' => '50px',
				'index' => 'can_server_howmany'
		) );
		$this->addColumn ( 'status', array (
				'header' => Mage::helper ( 'book' )->__ ( 'Status' ),
				'align' => 'right',
				'width' => '50px',
				'index' => 'status'
		) );
		
		$this->addExportType ( '*/*/exportCsv', Mage::helper ( 'book' )->__ ( 'CSV' ) );
		$this->addExportType ( '*/*/exportXml', Mage::helper ( 'book' )->__ ( 'Excel XML' ) );
		return parent::_prepareColumns ();
	}
	
	public function getRowUrl($row) {
		return $this->getUrl ( '*/*/edit', array (
				'id' => $row->getId () 
		) );
	}
	
	protected function _prepareMassaction() {
		$this->setMassactionIdField ( 'id' );
		$this->getMassactionBlock ()->setFormFieldName ( 'id' );
		$this->getMassactionBlock ()->setUseSelectAll ( true );
	
	
		$this->getMassactionBlock ()->addItem ( 'delete', array (
				'label' => Mage::helper ( 'book' )->__ ( 'Delete' ),
				'url' => $this->getUrl ( '*/adminhtml_rule/delete' )
		) );
	
	
	}
}

