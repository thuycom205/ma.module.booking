<?php

class HN_Book_Block_Adminhtml_Resource_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
	protected  $_databook ;
	protected function _prepareForm()
	{
		$form = new Varien_Data_Form(array('id' => 'addgiftvoucher', 'action' => $this->getData('action'), 'method' => 'post', 'enctype' => 'multipart/form-data'));

		$this->_databook = Mage::getSingleton('adminhtml/session')->getFormData();
		
		$editModel = Mage::registry('bresource_data');
		$this->_databook = $editModel->getData();

		$this->setForm($form);
		$note = "Pattern examples <br/><strong>[A.8] : 8 alpha chars<br>[N.4] : 4 numerics<br>[AN.6] : 6 alphanumeric<br>GIFT-[A.4]-[AN.6] : GIFT-ADFA-12NF0O</strong>";

		$fieldset = $form->addFieldset('book_form', array('legend'=>Mage::helper('book')->__('Resource Information')));

		$fieldset->addField('name', 'text', array(
		'label' => Mage::helper('book')->__('Resource Name'),
		'class' => 'grey',
		'name' => 'name',
		));
		
		$resource_type_option = Mage::getStoreConfig('book/general/resource_type');
		
		$option_value = '';
		$option_label = '';
		
		$rt_op = array();
		
		if ($resource_type_option) {
			
			$rs_type_arr = explode('|',$resource_type_option);
			
			if ($rs_type_arr) {
				foreach ($rs_type_arr as $option) {
					$option_label = $option;
					trim($option);
					$option_value = str_replace(' ', '_', $option);
					$rt_op[$option_value]=$option_label;
					
				}
				
			}
			
		}
		
		$fieldset->addField('type', 'select', array(
				'label' => Mage::helper('book')->__('Resource Type'),
				'class' => 'grey',
				'name' => 'type',
				'options' =>$rt_op
		));
		
		//if the resource is mandatory for booking,without it we can not have booking
		//for example room is mandatory resource for hotel booking
		
		$fieldset->addField('is_mandatory', 'checkbox', array(
				'label' => Mage::helper('book')->__('Is mandatory for booking'),
				'class' => 'grey',
				'name' => 'is_mandatory',
		));

        //add the description
        
		$fieldset->addField('description', 'textarea', array(
				'label' => Mage::helper('book')->__('Resource Description'),
				'class' => 'description',
				'name' => 'description',
		));
		
		$fieldset->addField('can_server_howmany', 'text', array(
				'label' => Mage::helper('book')->__('Can server how many booked block'),
				'class' => 'description',
				'name' => 'can_server_howmany',
		));
		
		
		$options = Mage::getModel('book/bresource') ->getStatusOption();

		$fieldset->addField('status', 'select', array(
            'name' => 'status',
            'label' => Mage::helper('catalog')->__('Status'),
            'title' => Mage::helper('catalog')->__('Status'),
            'values' => $options,
		));

		if (isset($this->_databook['filetype'])) {
			if ($this->_databook['filetype'] == HN_Pin_Model_Pin::TEXT_TYPE) {
				$decryptPin = Mage::helper('core')->decrypt($this->_databook['book_number']);
				
				$fieldset->addField('book_number', 'text', array(
						'label' => Mage::helper('book')->__('PIN number'),
						'class' => 'required-entry',
						'name' => 'book_number',
				));

				$this->_databook['book_number'] = $decryptPin;
			}

		}
		$allStores = Mage::app()->getStores();
		$store_option = array();
		$store_name = array();
		$store_name[] = "All store view";
		foreach ($allStores as $_eachStoreId => $val)  {
			$store_option[] = Mage::app()->getStore($_eachStoreId)->getId();
			$store_name[] = Mage::app()->getStore($_eachStoreId)->getName();
		}


		//end of shipbookg method
		if ( $this->_databook )
		{
			$form->setValues($this->_databook);
		} 

			
		return parent::_prepareForm();
	}

// 	protected function _toHtml() {
// 		$html = parent::_toHtml();

// 		if (isset($this->_databook['id'])) {

// 			if ($this->_databook['filetype'] != HN_Pin_Model_Pin::TEXT_TYPE ) {
// 				$html_plus = "<a href='" . Mage::getUrl('book/adminhtml_book/viewfilebook' ,array('id' =>$this->_databook['id']) )  ."' >". "View File" . "</a>";
// 				$html =$html.$html_plus;
// 			}
// 		}

// 		return $html;
// 	}

}

