<?php
class HN_Book_Block_Adminhtml_Resource_Resource extends Mage_Adminhtml_Block_Widget_Grid_Container
{
	public function __construct()
	{
		$this->_controller = 'adminhtml_resource';
		$this->_blockGroup = 'book';
		$this->_headerText = Mage::helper('book')->__('Booking Resource management');
		$this->_addButtonLabel = Mage::helper('book')->__('Add');

		parent::__construct();
	}
	
	
	
}
