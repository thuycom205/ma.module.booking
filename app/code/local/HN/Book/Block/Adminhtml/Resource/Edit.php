<?php
class HN_Book_Block_Adminhtml_Resource_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
	public function __construct()
	{
		parent::__construct();
		$this->_objectId = 'id';
		$this->_blockGroup = 'book';
		$this->_controller = 'adminhtml_resource';
	}
	public function getHeaderText()
	{
		if( Mage::registry('pin_data') && Mage::registry('pin_data')->getId() ) {
		return Mage::helper('book')->__("Edit", $this->htmlEscape(Mage::registry('pin_data')->getTitle()));
	} 
	else {
		return Mage::helper('book')->__('Add');
	}
	}
}
