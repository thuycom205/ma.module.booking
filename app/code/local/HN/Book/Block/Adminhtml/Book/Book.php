<?php 
class HN_Book_Block_Adminhtml_Book_Book extends Mage_Adminhtml_Block_Widget_Grid_Container
{
public function __construct()
	{
	
		
		$this->_controller = 'adminhtml_book';
		$this->_blockGroup = 'book';
		$this->_headerText = Mage::helper('book')->__('Booking management');
		$this->_addButtonLabel = Mage::helper('book')->__('Add');
		
		
		
		parent::__construct();
	}
}
