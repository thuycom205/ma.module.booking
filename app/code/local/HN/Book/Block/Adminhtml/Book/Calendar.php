<?php

class HN_Book_Block_Adminhtml_Book_Calendar extends Mage_Adminhtml_Block_Template {
	public function __construct() {
		$template = 'hn/book/calendar/calendar.phtml';
		$this->setTemplate ( $template );
		parent::__construct ();
	}
}