<?php
/**
 * Created by Magenest
 * User: Luu Thanh Thuy
 * Date: 25/02/2016
 * Time: 11:13
 */
class HN_Book_Block_Adminhtml_Book_Availability extends Mage_Adminhtml_Block_Template
{
    public function __construct() {
        $template = 'hn/book/global/availability.phtml';
        $this->setTemplate ( $template );
        parent::__construct ();
    }

    public function getPostUrl() {
        return $this->getUrl('book_admin/adminhtml_global/save/');
    }

    public function getGlobalAvaibility() {
       return  Mage::getModel('book/avarule')->getCollection();
    }
}