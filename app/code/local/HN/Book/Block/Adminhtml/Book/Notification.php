<?php
/**
 * Created by Magenest
 * User: Luu Thanh Thuy
 * Date: 26/02/2016
 * Time: 16:27
 */

class HN_Book_Block_Adminhtml_Book_Notification extends Mage_Adminhtml_Block_Template
{
    public function __construct() {
        $template = 'hn/book/global/notification.phtml';
        $this->setTemplate ( $template );
        parent::__construct ();
    }
}