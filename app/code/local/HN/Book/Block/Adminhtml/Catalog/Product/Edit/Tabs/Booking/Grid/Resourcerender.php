<?php 
class HN_Book_Block_Adminhtml_Catalog_Product_Edit_Tabs_Booking_Grid_Resourcerender extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
	public function render(Varien_Object $row)
	{

        $id = $row->getId();
        $resource_collection = Mage::getModel('book/brrelation')->getCollection()->addFieldToFilter('book_id' , $id);

		if ($resource_collection->getSize() > 0) {
			foreach ($resource_collection as $model) {
				$model = Mage::getModel('book/bresource')->load($model->getData('resource_id'));
				$name = $model->getName();
				
				$avatar = $model->getAvatar();
				
				$detail_link = Mage::helper("adminhtml")->getUrl('book_admin/adminhtml_resource/edit', array('id' =>$id));
				
				
				$un_assign_link = Mage::helper("adminhtml")->getUrl('book_admin/adminhtml_book/unassign', array('id' =>$row->getId() , 'rs_id' =>$model->getId()));
				echo "<span class='booking_resource'>{$name} </span>";
				
				?>
				<div class="hidden assigned-booking-resource">
				   <?php if ($avatar)   {  ?>  <img class="booking-resource-avatar" src="<?php echo  $avatar ?>" />  <?php  } ?>
				   
				   <div>  <a href="<?php echo $detail_link ?>" ><?php echo __('Detail') ?>   </a> </div>
				   
				    <div> <a href="<?php echo $un_assign_link ?>" ><?php echo __('Un assign') ?> </a></div>
				</div> 
				
				<?php 
			}
			
		}
		
	}
}