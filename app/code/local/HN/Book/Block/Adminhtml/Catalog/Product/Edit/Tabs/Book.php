<?php
//HN_Book_Block_Adminhtml_Catalog_Product_Edit_Tabs_Book
class HN_Book_Block_Adminhtml_Catalog_Product_Edit_Tabs_Book
    extends Mage_Adminhtml_Block_Widget implements Mage_Adminhtml_Block_Widget_Tab_Interface
{

    /**
     * Reference to product objects that is being edited
     *
     * @var Mage_Catalog_Model_Product
     */
    protected $_product = null;

    protected $_config = null;

    /**
     * Class constructor
     *
     */
    public function __construct()
    {
        parent::__construct();
//        $this->setSkipGenerateContent(true);
        $this->setTemplate('hn/book/catalog/tab/booking.phtml');
    }

    /**
     * Get tab URL
     *
     * @return string
     */
//    public function getTabUrl()
//    {
//        return $this->getUrl('downloadable/product_edit/form', array('_current' => true));
//    }

    /**
     * Get tab class
     *
     * @return string
     */
//    public function getTabClass()
//    {
//        return 'ajax';
//    }

    /**
     * Check is readonly block
     *
     * @return boolean
     */
    public function isReadonly()
    {
        return $this->getProduct()->getDownloadableReadonly();
    }

    /**
     * Retrieve product
     *
     * @return Mage_Catalog_Model_Product
     */
    public function getProduct()
    {
        return Mage::registry('current_product');
    }
    
    public function _prepareLayout() {
    	$this->setChild('bookingGrid', $this->getLayout()->createBlock('book/adminhtml_catalog_product_edit_tabs_booking_grid', 'bookingGrid'));
    
    	parent::_prepareLayout();
    }

    /**
     * Get tab label
     *
     * @return string
     */
    public function getTabLabel()
    {
        return Mage::helper('downloadable')->__('Booking General');
    }

    /**
     * Get tab title
     *
     * @return string
     */
    public function getTabTitle()
    {
        return Mage::helper('downloadable')->__('Booking General');
    }

    /**
     * Check if tab can be displayed
     *
     * @return boolean
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * Check if tab is hidden
     *
     * @return boolean
     */
    public function isHidden()
    {
        return false;
    }

    /**
     * Render block HTML
     *
     * @return string
     */
//     protected function _toHtml()
//     {
//     	$id=1;
//     	$model = Mage::getModel('giftcert/giftcert')->load(1);
		
// 		if ($model->getId() || $id == 0) {
// 			$data = Mage::getSingleton('adminhtml/session')->getFormData(true);
// 			if (!empty($data)) {
// 				$model->setData($data);
// 			}
// 		}
//             $_SESSION['gcedit'] = true;
//             Mage::register('giftcert_data', $model);
// 			Mage::register('voucherid', $id);
//         $accordion = $this->getLayout()->createBlock('adminhtml/widget_accordion')
//             ->setId('downloadableInfo');

//         $accordion->addItem('samples', array(
//             'title'   => Mage::helper('adminhtml')->__('Samples'),
//             'content' => $this->getLayout()
//                 ->createBlock('giftcert/adminhtml_giftcert_edit_tab_transactions')->toHtml(),
//             'open'    => false,
//         ));
// //
// //        $accordion->addItem('links', array(
// //            'title'   => Mage::helper('adminhtml')->__('Links'),
// //            'content' => $this->getLayout()->createBlock(
// //                'downloadable/adminhtml_catalog_product_edit_tab_downloadable_links',
// //                'catalog.product.edit.tab.downloadable.links')->toHtml(),
// //            'open'    => true,
// //        ));
// //
//      $this->setChild('accordion', $accordion);
//     var_dump($this->getLayout()
//                 ->createBlock('giftcert/adminhtml_giftcert_edit_tab_transactions')->toHtml());
//         return parent::_toHtml();
//     }

}
