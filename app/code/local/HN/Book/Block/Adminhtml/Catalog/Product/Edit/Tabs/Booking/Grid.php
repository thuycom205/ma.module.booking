<?php
//HN_Book_Block_Adminhtml this will be display in Booking tab of product edit page in backend area
class HN_Book_Block_Adminhtml_Catalog_Product_Edit_Tabs_Booking_Grid extends Mage_Adminhtml_Block_Widget_Grid {
	public function __construct() {
		parent::__construct ();
		$this->setId ( 'ticketGrid' );
		$this->setDefaultSort ( 'id' );
		$this->setDefaultDir ( 'ASC' );
		$this->setSaveParametersInSession ( true );
	}
	protected function _prepareCollection() {
		$collection = Mage::getModel ( 'book/book' )->getCollection ();
		$this->setCollection ( $collection );
		return parent::_prepareCollection ();
	}
	protected function _prepareColumns() {
		
		$this->addColumn ( 'id', array (
				'header' => Mage::helper ( 'book' )->__ ( 'ID' ),
				'align' => 'right',
				'width' => '50px',
				'index' => 'id' 
		) );
		
		$this->addColumn ( 'title', array (
				'header' => Mage::helper ( 'book' )->__ ( 'Title' ),
				'align' => 'right',
				'width' => '50px',
				'index' => 'title' 
		) );
		
		$this->addColumn ( 'order_id', array (
				'header' => Mage::helper ( 'book' )->__ ( 'Order ID' ),
				'align' => 'right',
				'width' => '50px',
				'index' => 'order_id' 
		) );
		
		$this->addColumn ( 'start_time', array (
				'header' => Mage::helper ( 'book' )->__ ('Start time' ),
				'align' => 'right',
				'width' => '50px',
				'index' => 'start_time' ,
				'date' =>'datetime'
		) );
		
		$this->addColumn ( 'end_time', array (
				'header' => Mage::helper ( 'book' )->__ ('End time' ),
				'align' => 'right',
				'width' => '50px',
				'index' => 'end_time' ,
				'date' =>'datetime'
		) );
		
		$this->addColumn ( 'status', array (
				'header' => Mage::helper ( 'book' )->__ ( 'Status' ),
				'align' => 'right',
				'width' => '50px',
				'index' => 'status' 
		) );
		$this->addColumn ( 'status', array (
				'header' => Mage::helper ( 'book' )->__ ( 'Status' ),
				'align' => 'right',
				'width' => '50px',
				'index' => 'status' 
		) );
		
		$this->addColumn ( 'resource_id', array (
				'header' => Mage::helper ( 'book' )->__ ( 'Resource' ),
				'align' => 'right',
				'width' => '50px',
				'index' => 'resource_id' ,
				'renderer'=> 'HN_Book_Block_Adminhtml_Catalog_Product_Edit_Tabs_Booking_Grid_Resourcerender'
		) );
		
		

		$this->addColumn ( 'customer_name', array (
				'header' => Mage::helper ( 'book' )->__ ( 'Customer Name' ),
				'align' => 'right',
				'width' => '50px',
				'index' => 'customer_name'
		) );
		
		/* add column to assign resource to booking*/
		$this->addColumn('assign_resource',
				array(
						'header'    => Mage::helper('book')->__('Assign Resource'),
						'width'     => '50px',
						'type'      => 'action',
						'getter'     => 'getId',
						'actions'   => array(
								array(
										'caption' => Mage::helper('book')->__('Assign Resource'),
										'url'     => array('base'=>'book_admin/adminhtml_book/assignresource'),
										'field'   => 'id'
								)
						),
						'filter'    => false,
						'sortable'  => false,
						'index'     => 'stores',
						'is_system' => true,
				));
		
		
		$this->addExportType ( '*/*/exportCsv', Mage::helper ( 'book' )->__ ( 'CSV' ) );
		$this->addExportType ( '*/*/exportXml', Mage::helper ( 'book' )->__ ( 'Excel XML' ) );
		return parent::_prepareColumns ();
	}
	public function getRowUrl($row) {
		return $this->getUrl ( '*/*/edit', array (
				'id' => $row->getId () 
		) );
	}
	protected function _prepareMassaction() {
		$this->setMassactionIdField ( 'id' );
		$this->getMassactionBlock ()->setFormFieldName ( 'id' );
		$this->getMassactionBlock ()->setUseSelectAll ( true );
	
	
		$this->getMassactionBlock ()->addItem ( 'delete', array (
				'label' => Mage::helper ( 'book' )->__ ( 'Delete' ),
				'url' => $this->getUrl ( '*/adminhtml_rule/delete' )
		) );
	
	
	}
}

