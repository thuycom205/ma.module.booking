<?php
class HN_Book_Block_Adminhtml_Catalog_Product_Edit_Tabs_Resource_Grid_Renderbook extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract {
	
	public function render(Varien_Object $row)
	{
		$value = $row->getData('book_id');
		if ($value) {
			return '<span class="associated-book">'.$value.'</span>';
		} 
	}
}