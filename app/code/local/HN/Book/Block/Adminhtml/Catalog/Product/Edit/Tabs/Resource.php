<?php
class HN_Book_Block_Adminhtml_Catalog_Product_Edit_Tabs_Resource
extends Mage_Adminhtml_Block_Widget implements Mage_Adminhtml_Block_Widget_Tab_Interface
{
	/**
	 * Reference to product objects that is being edited
	 *
	 * @var Mage_Catalog_Model_Product
	 */
	protected $_product = null;
	
	protected $_config = null;
	
	/**
	 * Class constructor
	 *
	 */
	public function __construct()
	{
		parent::__construct();
		//        $this->setSkipGenerateContent(true);
		$this->setTemplate('hn/book/catalog/tab/resource.phtml');
		
	}
	
	/**
	 * Retrieve product
	 *
	 * @return Mage_Catalog_Model_Product
	 */
	public function getProduct()
	{
		return Mage::registry('current_product');
	}
	
	public function _prepareLayout() {
		$this->setChild('userGrid', $this->getLayout()->createBlock('book/adminhtml_catalog_product_edit_tabs_resource_grid', 'roleUsersGrid'));
		
		parent::_prepareLayout();
	}
	
	public function getTabLabel()
	{
		return Mage::helper('downloadable')->__('Booking Resource');
	}
	
	/**
	 * Get tab title
	 *
	 * @return string
	 */
	public function getTabTitle()
	{
		return Mage::helper('downloadable')->__('Booking Resource');
	}
	
	/**
	 * Check if tab can be displayed
	 *
	 * @return boolean
	 */
	public function canShowTab()
	{
		return true;
	}
	
	/**
	 * Check if tab is hidden
	 *
	 * @return boolean
	 */
	public function isHidden()
	{
		return false;
	}
	
}