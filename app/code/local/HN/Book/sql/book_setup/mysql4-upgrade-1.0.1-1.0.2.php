<?php
/* @var $this Mage_Catalog_Model_Resource_Eav_Mysql4_Setup */

$installer = $this;
/* @var $installer Mage_Catalog_Model_Resource_Eav_Mysql4_Setup */

$installer->startSetup();
$attr = array (
		'group'             => 'General',
		'attribute_model' => NULL,
		'backend' => '',
		'type' => 'int',
		'table' => '',
		'frontend' => '',
		'input' => 'select',
		'label' => 'Booking block type',
		'frontend_class' => '',
		'source' => '',
		'required' => '0',
		'user_defined' => '1',
		'default' => '',
		'unique' => '0',
		'note' => '',
		'input_renderer' => NULL,
		'global' => '1',
		'visible' => '1',
		'searchable' => '1',
		'filterable' => '1',
		'comparable' => '1',
		'visible_on_front' => '0',
		'is_html_allowed_on_front' => '0',
		'is_used_for_price_rules' => '1',
		'filterable_in_search' => '1',
		'used_in_product_listing' => '0',
		'used_for_sort_by' => '0',
		'is_configurable' => '1',
		'apply_to' => 'book',
		'visible_in_advanced_search' => '1',
		'position' => '1',
		'wysiwyg_enabled' => '0',
		'used_for_promo_rules' => '1',
		'option' =>
		array (
				'values' =>
				array (
						0 => 'Month',
						1 => 'Days',
						2 => 'Hour',
						3 => 'Min',
						
				),
		),
);
$this->addAttribute('catalog_product','magenest_booktime_type',$attr);
$installer->endSetup();