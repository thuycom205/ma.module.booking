<?php


$installer = $this;
/* @var $installer Mage_Catalog_Model_Resource_Eav_Mysql4_Setup */

$installer->startSetup();

$installer->run("
		ALTER TABLE  {$this->getTable('book/book')}  ADD  `adult_amount` int(11) NULL;
		ALTER TABLE  {$this->getTable('book/book')}  ADD  `child_amount` int(11) NULL;

		");
$installer->endSetup();