<?php
$installer = $this;
/* @var $installer Mage_Catalog_Model_Resource_Eav_Mysql4_Setup */

$installer->startSetup();

$installer->run("
		ALTER TABLE  {$this->getTable('book/bresource')}  ADD  `is_auto_assign` varchar(255) NULL;

		");
$installer->endSetup();