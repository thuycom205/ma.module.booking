<?php
/* @var $this Mage_Catalog_Model_Resource_Eav_Mysql4_Setup */

$installer = $this;
/* @var $installer Mage_Catalog_Model_Resource_Eav_Mysql4_Setup */

$installer->startSetup();

$installer->run("

		DROP TABLE IF EXISTS {$this->getTable('book/brrelation')};

		CREATE TABLE {$this->getTable('book/brrelation')} (
		`id` int(11) unsigned NOT NULL auto_increment,
		`book_id` varchar(255) NOT NULL default '',
		`resource_id` int(11) NOT NULL default 0 ,
		PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

");

$installer->endSetup();