<?php
/** code to create product attribute 
 * 1. bookable product require resource
 * 2.Assign resource manually or automatically
 *  */
$installer->addAttribute('catalog_product', 'magenest_bresource_require', array(
		'group'             => 'General',
		'type'              => 'int',
		'backend'           => '',
		'frontend'          => '',
		'label'             => 'Resource is mandatory for resource',
		'input'             => 'boolean',
		'class'             => '',
		'source'            => '',
		'is_global', Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
		'visible'           => true,
		'required'          => false,
		'user_defined'      => false,
		'default'           => '0',
		'searchable'        => false,
		'filterable'        => false,
		'comparable'        => false,
		'visible_on_front'  => false,
		'unique'            => false,
		'apply_to'          => 'book',
		'is_configurable'   => false,
		'used_in_product_listing', '1'
));

$installer->addAttribute('catalog_product', 'magenest_bresource_assignau', array(
		'group'             => 'General',
		'type'              => 'int',
		'backend'           => '',
		'frontend'          => '',
		'label'             => 'Resource is automatically assign',
		'input'             => 'boolean',
		'class'             => '',
		'source'            => '',
		'is_global', Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
		'visible'           => true,
		'required'          => false,
		'user_defined'      => false,
		'default'           => '0',
		'searchable'        => false,
		'filterable'        => false,
		'comparable'        => false,
		'visible_on_front'  => false,
		'unique'            => false,
		'apply_to'          => 'book',
		'is_configurable'   => false,
		'used_in_product_listing', '1'
));