<?php
/* @var $this Mage_Catalog_Model_Resource_Eav_Mysql4_Setup */

$installer = $this;
/* @var $installer Mage_Catalog_Model_Resource_Eav_Mysql4_Setup */

$installer->startSetup();

$installer->addAttribute('catalog_product', 'magenest_multidate_book', array(
		'group'             => 'General',
		'type'              => 'int',
		'backend'           => '',
		'frontend'          => '',
		'label'             => 'Enable multiple date choose',
		'input'             => 'boolean',
		'class'             => '',
		'source'            => '',
		'is_global', Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
		'visible'           => true,
		'required'          => false,
		'user_defined'      => false,
		'default'           => '0',
		'searchable'        => false,
		'filterable'        => false,
		'comparable'        => false,
		'visible_on_front'  => false,
		'unique'            => false,
		'apply_to'          => 'book',
		'is_configurable'   => false,
		'used_in_product_listing', '1'
));

$installer->addAttribute('catalog_product', 'magenest_startertime_book', array(
		'group'             => 'General',
		'type'              => 'int',
		'backend'           => '',
		'frontend'          => '',
		'label'             => 'Enable selector for begin time',
		'input'             => 'boolean',
		'class'             => '',
		'source'            => '',
		'is_global', Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
		'visible'           => true,
		'required'          => false,
		'user_defined'      => false,
		'default'           => '0',
		'searchable'        => false,
		'filterable'        => false,
		'comparable'        => false,
		'visible_on_front'  => false,
		'unique'            => false,
		'apply_to'          => 'book',
		'is_configurable'   => false,
		'used_in_product_listing', '1'
));

$installer->addAttribute('catalog_product', 'magenest_endtime_book', array(
		'group'             => 'General',
		'type'              => 'int',
		'backend'           => '',
		'frontend'          => '',
		'label'             => 'Enable selector for ending time',
		'input'             => 'boolean',
		'class'             => '',
		'source'            => '',
		'is_global', Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
		'visible'           => true,
		'required'          => false,
		'user_defined'      => false,
		'default'           => '0',
		'searchable'        => false,
		'filterable'        => false,
		'comparable'        => false,
		'visible_on_front'  => false,
		'unique'            => false,
		'apply_to'          => 'book',
		'is_configurable'   => false,
		'used_in_product_listing', '1'
));

$installer->addAttribute('catalog_product', 'magenest_reconfirmation_book', array(
		'group'             => 'General',
		'type'              => 'int',
		'backend'           => '',
		'frontend'          => '',
		'label'             => 'Require confirmation',
		'input'             => 'boolean',
		'class'             => '',
		'source'            => '',
		'is_global', Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
		'visible'           => true,
		'required'          => false,
		'user_defined'      => false,
		'default'           => '0',
		'searchable'        => false,
		'filterable'        => false,
		'comparable'        => false,
		'visible_on_front'  => false,
		'unique'            => false,
		'apply_to'          => 'book',
		'is_configurable'   => false,
		'used_in_product_listing', '1'
));


$installer->addAttribute('catalog_product', 'magenest_cancellation_book', array(
		'group'             => 'General',
		'type'              => 'int',
		'backend'           => '',
		'frontend'          => '',
		'label'             => 'Can be cancelled',
		'input'             => 'boolean',
		'class'             => '',
		'source'            => '',
		'is_global', Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
		'visible'           => true,
		'required'          => false,
		'user_defined'      => false,
		'default'           => '0',
		'searchable'        => false,
		'filterable'        => false,
		'comparable'        => false,
		'visible_on_front'  => false,
		'unique'            => false,
		'apply_to'          => 'book',
		'is_configurable'   => false,
		'used_in_product_listing', '1'
));
//$installer->updateAttribute('catalog_product', 'inchoo_featured_product', 'is_global', '1');
$installer->endSetup();