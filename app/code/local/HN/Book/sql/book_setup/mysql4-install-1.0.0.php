<?php
/* @var $this Mage_Catalog_Model_Resource_Eav_Mysql4_Setup */

$installer = $this;
/* @var $installer Mage_Catalog_Model_Resource_Eav_Mysql4_Setup */

$installer->startSetup();

$fieldList = array(
		'price',
		'cost',
		'weight',
		'tax_class_id'
);

// make these attributes applicable to bookable products

 foreach ($fieldList as $field) {
 	$applyTo = explode(',', $installer->getAttribute(Mage_Catalog_Model_Product::ENTITY, $field, 'apply_to'));
 	if (!in_array('book', $applyTo)) {
 		$applyTo[] = 'book';
 		$installer->updateAttribute(Mage_Catalog_Model_Product::ENTITY, $field, 'apply_to', join(',', $applyTo));
 	}
 }

$installer->run("

		DROP TABLE IF EXISTS {$this->getTable('book/book')};

		CREATE TABLE {$this->getTable('book/book')} (
		`id` int(11) unsigned NOT NULL auto_increment,
		`title` varchar(255) NOT NULL default '',
		`order_id` int(11) NOT NULL default 0 ,
		`order_item_id` int(11) NOT NULL default 0 ,
		`product_id` varchar(255) NOT NULL default '',
		`product_name` text NOT NULL default '',
		`status` smallint(6) NOT NULL default '0',
		`customer_id` int not null default 0,
		`customer_name` varchar(255) not null default ' ',
		`info` text null,
		`start_time` datetime NULL,
		`end_time` datetime NULL,
		`created_time` datetime NULL,
		`update_time` datetime NULL,
		PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS {$this->getTable('book/bookmeta')};

		CREATE TABLE {$this->getTable('book/bookmeta')} (
		`id` int(11) unsigned NOT NULL auto_increment,
		`book_id` varchar(255) NOT NULL default '',
		`meta_key` varchar(255) NOT NULL default '',
		`meta_value` text null ,
		PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS {$this->getTable('book/pricerule')};

		CREATE TABLE {$this->getTable('book/pricerule')} (
		`id` int(11) unsigned NOT NULL auto_increment,
		`product_id` int(11) NOT NULL default 0,
		`time_type` varchar(255) NOT NULL default '',
		`time_from` varchar(255) NOT NULL default '',
		`time_to` varchar(255) NOT NULL default '',
		`price` int(11) NOT NULL default 0,
		`init_fee` int(11)  NULL default 0,
		`block_fee` int(11)  NULL default 0,
		PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS {$this->getTable('book/avarule')};

		CREATE TABLE {$this->getTable('book/avarule')} (
		`id` int(11) unsigned NOT NULL auto_increment,
		`product_id` int(11) NOT NULL default 0,
		`time_type` varchar(255) NOT NULL default '',
		`time_from` varchar(255) NOT NULL default '',
		`time_to` varchar(255) NOT NULL default '',
		`ava` int(11) NOT NULL default 0,
		PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS {$this->getTable('book/bresource')};
		
CREATE TABLE {$this->getTable('book/bresource')} (
`id` int(11) unsigned NOT NULL auto_increment,
`name` varchar(255) NOT NULL default '',
`description` text NOT NULL default '',
`priority` int(11) NULL default 0,
`can_server_howmany` int(11) NOT NULL default 0,
`book_id` int(11)  NULL default 0,
`product_id` int(11)  NULL default 0,
`status` varchar(255) NOT NULL default '',
PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS {$this->getTable('book/bresourcemeta')};

CREATE TABLE {$this->getTable('book/bresourcemeta')} (
`id` int(11) unsigned NOT NULL auto_increment,
`resource_id` varchar(255) NOT NULL default '',
`meta_key` varchar(255) NOT NULL default '',
`meta_value` text null ,
PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
");


$installer->endSetup();