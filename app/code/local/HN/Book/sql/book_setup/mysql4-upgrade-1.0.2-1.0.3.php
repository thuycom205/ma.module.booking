<?php
/* @var $this Mage_Catalog_Model_Resource_Eav_Mysql4_Setup */

$installer = $this;

$installer->startSetup();

$installer->run("
DROP TABLE IF EXISTS {$this->getTable('book/bresource')};
		
CREATE TABLE {$this->getTable('book/bresource')} (
`id` int(11) unsigned NOT NULL auto_increment,
`name` varchar(255) NOT NULL default '',
`description` text NOT NULL default '',
`priority` int(11) NULL default 0,
`can_server_howmany` int(11) NOT NULL default 0,
`book_id` int(11)  NULL default 0,
`product_id` int(11)  NULL default 0,
`status` varchar(255) NOT NULL default '',
PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS {$this->getTable('book/bresourcemeta')};

CREATE TABLE {$this->getTable('book/bresourcemeta')} (
`id` int(11) unsigned NOT NULL auto_increment,
`resource_id` varchar(255) NOT NULL default '',
`meta_key` varchar(255) NOT NULL default '',
`meta_value` text null ,
PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
");


$installer->endSetup();