<?php
$installer = $this;
/* @var $installer Mage_Catalog_Model_Resource_Eav_Mysql4_Setup */

$installer->startSetup();

$installer->run("
		ALTER TABLE  {$this->getTable('book/bresource')}  ADD  `type` varchar(255) NULL;
		ALTER TABLE  {$this->getTable('book/bresource')}  ADD  `is_mandatory` varchar(255) NULL;
		ALTER TABLE  {$this->getTable('book/book')}  ADD  `estimate_arrive_time` varchar(255) NULL;

		");
$installer->endSetup();